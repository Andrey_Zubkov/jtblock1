
jQuery(document).ready(function($) {

	$('.jtblock-slider .item').height($(window).height() * .75);
	$(window).on('resize', function(){
		$('.jtblock-slider .item').height($(window).height() * .75);
	});

	$('.jtblock-slider').slick({
		infinite: true,
	  	slidesToShow: 4,
	  	slidesToScroll: 1,
		autoPlay: true,
		speed: 1300,
		dots: false,
		arrows: true,
		nextArrow: '<div class="slider-arrows next"><i class="fa fa-arrow-right"></i></div>',
		prevArrow: '<div class="slider-arrows prev"><i class="fa fa-arrow-left"></i></div>',
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
				}
		    },
		    {
				breakpoint: 991,
				settings: {
					slidesToShow: 2,
				}
		    },
		],
	});
});
