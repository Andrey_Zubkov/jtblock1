<div class="panel">

	<div class="panel-heading"><i class="icon icon-tags"></i> {l s='Settings' mod='block1'}</div>

	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="panel-body">
				<div class="header-list-photos">
					<div class="actions">
						<ul>
							<li>
								<a class="add_photo toolbar_btn  pointer"><i class="process-icon-new"></i></a>
								<div>{l s='Add item' mod='block1'}</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="list-photos" id="accordion" role="tablist" aria-multiselectable="true">

					{foreach from=$result key=number item=value}
						<div class="container-photo">
							<div class="container-title" role="tab" id="headingOne">
								<div class="number">{$number + 1}</div>
								<div class="img"><img src="{if $value.src_img}{$value.src_img}{/if}" alt="" />
								</div>
								<div class="title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{$number}" aria-expanded="false" aria-controls="collapse{$number}">
										{if $value['name']}{$value['name']}{/if}
									</a>
								</div>
								<div class="actions">
									<ul>
										<li>
											<form action="" method="post">
												<input type="hidden" name="item_id" value="{$value.id}"/>
												<input type="hidden" name="item_token" value="{$token}" />
												<button type="submit" value="1" name="submitJtblock1ModuleDelete" class="btn btn-default">
													<i class="icon-trash"></i> {l s='Delete' mod='block1'}
												</button>
											</form>
										</li>
									</ul>
								</div>
							</div>
							<div id="collapse{$number}" class="content panel-collapse collapse" role="tabpanel">

								<form action="" method="post" enctype="multipart/form-data">

									<!-- hidden -->
									<input type="hidden" name="item_id" class="form-control" value="{$value.id}"/>
									<input type="hidden" name="img_item" class="form-control" value="{$value.src_img}"/>
									<input type="hidden" name="item_token" value="{$token}" />
									<!-- /hidden -->

									<!-- name -->
									<div class="form-group">
										<label for="item_name">{l s='Name' mod='block1'}</label>
										<input type="text" class="form-control" id="item_name" name="item_name" placeholder="{l s='Enter the item name' mod='block1'}" value="{$value.name}">
									</div>
									<!-- /name -->

									<!-- title 
									<div class="form-group">
										<label for="item_title">{l s='Title' mod='block1'}</label>
										<input type="text" class="form-control" id="item_title" name="item_title" placeholder="{l s='Enter the item title' mod='block1'}" value="{$value.title}">
									</div>
									/title --> 

									<!-- link -->
									<div class="form-group">
										<label for="item_link">{l s='Link' mod='jtgallery'}</label>
										<input type="text" id="item_link" name="item_link" placeholder="{l s='Enter the item link' mod='block1'}" value="{$value.link}">
									</div>
									<!-- /link -->

									<!-- desc 
									<div class="form-group">
										<label for="item_description">{l s='Description' mod='jtgallery'}</label>
										<textarea id="item_description" name="item_description" placeholder="{l s='Enter the item description' mod='block1'}">{$value.description}
										</textarea>
									</div>
									desc -->
									<!-- date insert -->
									<div class="form-group">
										<label for="date_ins">{l s='Date insert' mod='jtgallery'}</label>
										<p class="date_ins">
											{$value.date_add}
										</p>
									</div>
									<!-- /date insert -->

									<!-- date update -->
									<div class="form-group">
										<label for="date_ins">{l s='Date update' mod='jtgallery'}</label>
										<p class="date_ins">
											{$value.date_upd}
										</p>
									</div>
									<!-- /date update -->

									<!-- img -->
									<div class="form-group">
										<label for="item_photo">{l s='Photo' mod='block1'}</label>
										<input name="item_photo" type="file" />
									</div>
									<!-- /img -->

									<button type="submit" value="1" name="submitJtblock1Module" class="btn btn-default">
										<i class="process-icon-save"></i> {l s='Save' mod='block1'}
									</button>
								</form>

							</div>
						</div>
					{/foreach}

				</div>
			</div>
			<div class="panel-footer">
				{$pagination}
			</div>
		</div>
	</div>
</div>

<script>
	jQuery(document).ready(function($){
		$('.add_photo').click(function(){
			var number = $('.list-photos').children().length + 1;
			console.log(number);
			$('.list-photos').prepend(`
            <div class="container-photo">
				<div class="title" role="tab" id="headingOne">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse` + number + `" aria-expanded="true" aria-controls="collapse` + number + `">
						{l s='New Item' mod='block1'}
					</a>
				</div>
				<div id="collapse` + number + `" class="content panel-collapse collapse in" role="tabpanel">

				<form action="" method="post" enctype="multipart/form-data">
					<input type="hidden" name="item_token" value="{$token}" />

					<!-- name -->
					<div class="form-group">
					<label for="item_name">{l s='Name' mod='jtgallery'}</label>
					<input type="text" class="form-control" id="item_name" name="item_name" placeholder="Введите название элемента" value="">
					</div>
					<!-- /name -->

					<!-- title 
					<div class="form-group">
					<label for="item_title">{l s='Title' mod='jtgallery'}</label>
					<input type="text" class="form-control" id="item_title" name="item_title" placeholder="Введите тайтл элемента" value="">
					</div>
					title -->

					<!-- link -->
					<div class="form-group">
					<label for="item_link">{l s='Link' mod='block1'}</label>
					<input type="text" id="item_link" name="item_link" placeholder="{l s='Enter the item link' mod='block1'}" value="">
					</div>
					<!-- /link -->

					<!-- desc 
					<div class="form-group">
						<label for="item_description">{l s='Description' mod='jtgallery'}</label>
						<textarea id="item_description" name="item_description" placeholder="{l s='Enter the item description' mod='block1'}">

						</textarea>
					</div>
					desc -->

					<!-- img -->
					<div class="form-group">
					<label for="item_photo">{l s='Photo' mod='иblock1'}</label>
					<input name="item_photo" type="file" />
					</div>
					<!-- /img -->

					<button type="submit" value="1" name="submitJtblock1Module" class="btn btn-default">
					<i class="process-icon-save"></i> {l s='Save' mod='block1'}
					</button>
					</form>

					</div>
					</div>
		`);
    });
});
</script>
