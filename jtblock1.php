<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Jtblock1 extends Module
{
    protected $config_form = false;

      /**
     * @var
     */
    protected $items;

    /**
     * @var
     */
    protected $offset;

    /**
     * @var
     */
    protected $current_page;


    public function __construct()
    {
        $this->name = 'jtblock1';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Jaguar-team';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('jtblockshop1');
        $this->description = $this->l('Here is my first block1');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('JTBLOCK1_LIVE_MODE', false);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayBackOfficeHeader')&&
            $this->registerHook('displayHome');
    }

    public function uninstall()
    {
        Configuration::deleteByName('JTBLOCK1_LIVE_MODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {   

        $output = '';
        $available = false;
        session_start();

        /** generate token */
        if (!isset($_SESSION['token'])) {
            $token = md5(md5(rand(1, mt_getrandmax()).rand(1, mt_getrandmax())));
            $_SESSION['token'] = $token;
        } else {
            $token = $_SESSION['token'];
            $token_input = Tools::getValue('item_token');
            $available = $token_input == $token;

            $token = md5(md5(rand(1, mt_getrandmax()).rand(1, mt_getrandmax())));
            $_SESSION['token'] = $token;
        }
        
        if (((bool)Tools::isSubmit('submitJtblock1Module')) == true  && $available) {
            if ($this->SaveItem($arr)) {
                $output .= $this->displayConfirmation($this->l('Item were successfully updated'));
            }                              
            
        }

        if (((bool)Tools::isSubmit('submitJtblock1ModuleDelete')) == true && $available) {
            if ($this->DeleteItem()) {
                $output .= $this->displayConfirmation($this->l('Item was successfully deleted'));
            }
        }

        $this->context->smarty->assign(array(
            'result'        => $this->_getDataTable(),
            'pagination'    => $this->getPagination(),
            'token'         => $token,));
        
        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitJtblock1Module';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'JTBLOCK1_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'JTBLOCK1_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'JTBLOCK1_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'JTBLOCK1_LIVE_MODE' => Configuration::get('JTBLOCK1_LIVE_MODE', true),
            'JTBLOCK1_ACCOUNT_EMAIL' => Configuration::get('JTBLOCK1_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'JTBLOCK1_ACCOUNT_PASSWORD' => Configuration::get('JTBLOCK1_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {   
        // libs
        $this->context->controller->addCSS($this->_path.'views/css/bootstrap.min.css', 'all');
        $this->context->controller->addCSS($this->_path.'views/css/bootstrap-theme.min.css', 'all');
        $this->context->controller->addCSS($this->_path.'views/css/font-awesome.min.css', 'all');
        $this->context->controller->addCSS($this->_path.'views/css/slick.css', 'all');
        $this->context->controller->addCSS($this->_path.'views/css/slick-theme.css', 'all');

        $this->context->controller->addJS($this->_path.'/views/js/slick.min.js');
        
        // custom
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayBackOfficeHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/back.css');
    }

    public function hookDisplayHome()
    {
        $this->context->smarty->assign(
            array(
                'data'  => $this->_getDataTable(),
            )
        );

        return $this->display(__FILE__, 'block1.tpl');
    }

    public function _getDataTable($all = false, $limit = 15)
    {
        if ($all) {
            $sql = 'SELECT * FROM '._DB_PREFIX_.'block1_jt';

            return Db::getInstance()->ExecuteS($sql);
        }

        $offset = $this->getOffset();
        $sql = 'SELECT * FROM '._DB_PREFIX_.'block1_jt 
                ORDER BY `id` DESC 
                LIMIT '.$limit.'
                OFFSET '.$offset;

        if (!$this->items)
            $this->photos = Db::getInstance()->ExecuteS($sql);

        return $this->photos;        
    }

    public function SaveItem()
    {
        $img = NULL;
        $table = 'block1_jt';

        if (Tools::getValue('img_item')) {
            $img = Tools::getValue('img_item');
        }

        if ($_FILES['item_photo']) {
            if (!is_dir(_PS_IMG_DIR_.'jtblock1/')) {
                mkdir(_PS_IMG_DIR_.'jtblock1/');
            }

            $file = $_FILES['item_photo'];
            $type = Tools::strtolower(Tools::substr(strrchr($file['name'], '.'), 1));
            $name = md5(md5(rand(1, mt_getrandmax()).rand(1, mt_getrandmax()).'jtblock1')).'.'.$type;
            $dir_upload = _PS_IMG_DIR_.'jtblock1/'.$name;

            if (move_uploaded_file($file['tmp_name'], $dir_upload)) {
                $img = _PS_BASE_URL_.__PS_BASE_URI__.'/img/jtblock1/'.$name;
            }
        }

        if (Tools::getValue('item_id')) {
            $data = array(
                'name'          => Tools::getValue('item_name'),
                /*'title'         => Tools::getValue('item_title'),*/
                'link'          => Tools::getValue('item_link'),
                /*'description'   => Tools::getValue('item_description'),*/
                'src_img'       => $img,
                );
            $data['date_upd'] = date('Y:m:d H:m:s');

            return Db::getInstance()->update(
                $table,
                $data,
                'id = '.Tools::getValue('item_id')
                );
        } else {
            $data = array(
                'name'          => Tools::getValue('item_name'),
                /*'title'         => Tools::getValue('item_title'),*/
                'link'          => Tools::getValue('item_link'),
                 /*'description'   => Tools::getValue('item_description'),*/
                'src_img'       => $img,
                );
            $data['date_add'] = date('Y:m:d H:m:s');
            $data['date_upd'] = date('Y:m:d H:m:s');

            return Db::getInstance()->insert($table, $data);    
        }
    }

    public function DeleteItem()
    {
        $id = Tools::getValue('item_id');
        $table = 'block1_jt';
        $where = 'id = '.$id;

        return Db::getInstance()->delete($table, $where);
    }

    protected function getOffset()
    {
        $page = (Tools::getValue('page') ? Tools::getValue('page') : 1);

        $this->offset = ($page == 1 ? 0 : ($page - 1) * 15);
        $this->current_page = $page;

        return $this->offset;
    }

    protected function getPagination()
    {
        $count_items = count($this->_getDataTable(true));

        if (!$count_items) {
            return false;
        }

        $count_pag = ceil($count_items / 15);

        if ($count_pag <= 1) {
            return false;
        } else {
            parse_str($_SERVER['QUERY_STRING'], $vars);

            $pagination = '<nav aria-label="Page navigation"><ul class="pagination">';
            for ($i = 1; $i <= $count_pag; $i++) {
                $class = ($this->current_page == $i ? 'class="active"' : '');
                $vars['page'] = $i;
                $link = '?'.http_build_query($vars);
                $pagination .= '<li '.$class.'><a href="'.$link.'">'.$i.'</a></li>';
            }

            $pagination .= '</ul></nav>';

            return $pagination;
        }

    }



}
